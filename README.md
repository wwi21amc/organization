# Organization

Organisatorisches für den Kurs WWI21AMC. Zusammen haben wir 25 VE, welche wir in 5 Sessions zu je 5 VE aufgeteilt haben.

## Prüfungsleistung

Insgesamt verteilen wir in diesem Kurs 100 Punkte (die werden dann auf die 60 Dualis-Punkte runterskaliert). Diese setzen sich zusammen aus:
- 5 Challenges zu je 20 Punkten, also 100 Punkte
- 10 Bonus-Punkte durch die Kahoots

### Challenges

Jede Session bekommt ihr von uns eine Challenge, welche bis zur nächsten Session zu erledigen ist. Diese findet ihr in `./challenges`.
Die Challenges bestehen meistens aus zwei Teilen: Einem konzeptuellen und einem praktischen Part. Beide Parts sind jeweils in unterschiedlichen Gruppen zu bearbeiten. Die Parts werden als Gruppe benotet.

### Kahoots

Jede Session fragen wir die Inhalte aus der vorherigen Session spielerisch mit einem Kahoot ab.
Wer in einem Kahoot mindestens 70% der Fragen richtig beantwortet hat, bekommt 2 Bonus-Punkte.
Wer am Ende des Kurses die meisten Fragen richtig beantwortet hat, kriegt Swag der diesjährigen [KubeCon](https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/).

Die Kahoots sind Bonus-Punkte und nicht Teil der offiziellen Prüfungsleistung. Wer das Kahoot verpasst, kann auch keine Bonus-Punkte erhalten.

## Wie wir arbeiten

Dieser Kurs wird teils remote und teils vor Ort veranstaltet. Zur asynchronen Kommunikation benutzen wir Slack. Aufgaben und Abgaben dokumentieren wir hier auf Gitlab.

Wir lassen euch viele Freiheiten bzgl. wie und wann ihr arbeitet. Wir erwarten von euch ausreichend Selbstständigkeit,
um die Arbeit im vorgegebenen Zeitrahmen zu erledigen und gleichmäßig in eurem Team aufzuteilen.

Lest mehr in unserem `./CODE_OF_CONDUCT.md`.
