# Challenge 4

Deadline: 2024-06-18

## Part 1: Build the Trading Frontend (20 pts)

Your task is to build a prototype frontend for your Trading API which must fulfill following functional requirements:

- Users can use the website to view the current market price for some cryptocurrencies.
- Users can create a new order through the website which gets posted to your Trading API.
- Users can view all orders currently stored in the Trading API.

Choose whatever technology you like. Think about whether server-side or client-side rendering would make more sense and implement it accordingly.
You also need to create a containerized web server that serves the website so that your testers can start the whole system as easily as possible.

Non-functional requirements:

- Your website startup configuration has been included in the docker-compose setup. Running `docker compose up` will start the webserver for the website, the Trading API and the database.
- When started through docker-compopse, the website can be reached on the local machine at `localhost:8080`.

Create a new repository called `challenge4` where you add the website alongside the Trading API.
You now basically have a so-called monorepo.

Note: You will most likely run into troubles with CORS. You may need to adapt the backend API to set the correct CORS headers. Learn more about CORS here: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

## Part 2: Authenticate your users (20 pts)

### Secure your frontend

You now need to implement a login mechanism for the website users. Adapt your frontend to fulfill following requirements.

- Users have the possibility to log in, either with username / email and password or through a third-party identity provider
- Only logged in users can create new orders
- All (unauthenticated) users can see all orders

As an all-in-one identity solution, we recommend using Google Cloud Identity Platform.
It directly integrates with third-party identity providers (Google, Apple, LinkedIn, etc.) and manages your user identities for you.
It's free, and you can use your existing Google Cloud projects for it.

However, there are many easy-to-use alternatives out there if you're feeling adventurous, for example [Supabase Auth](https://supabase.com/docs/guides/auth).

### Secure your backend

So far you have only restricted order creation in your frontend, but that does not keep unauthenticated users from directly
performing requests to your Trading API using tools like Postman. You now need to protect your API, so that only authenticated users
can use the `POST /orders` endpoint.

Requirements:
- The `POST /orders` endpoint validates that only users that logged in in your frontend can create orders.
- The `GET /orders` endpoint does not require any authentication.
- You adapt the internal data model of the backend to also store the user IDs of orders.
- The `GET /orders` endpoint now returns the creator's user ID for each order.
- You modify the OpenAPI specification accordingly.

Make sure your system is not subsceptible to CSRF and XSS attacks.
If you need a refresher on that, see [CSRF Demo](https://victorzhou.com/blog/csrf/) and [XSS Demo](https://victorzhou.com/blog/xss/)

Commit your changes to the `challenge4` repository. As before, running `docker compose up` needs to start all components of the
system.

> Be prepared to present your setup on our next and last session on 2024-06-18.