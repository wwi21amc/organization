# Tests for Challenge 1

To test whether your Trading API implementation is compliant with the OpenAPI specification (at least to our expectations),
you can use these tests.

To run the tests, you will need to install [hurl.dev](https://hurl.dev/).

Run the tests by executing

```
hurl test.hurl
```

where `test.hurl` is the path to the test file provided in this repository.

Make sure your Trading API instance is reachable on `http://localhost:3000`.

If a test fails, the output of hurl will look like this:

```
error: Assert status code
  --> test.hurl:69:6
   |
   | POST http://localhost:3000/orders
   | ...
69 | HTTP 406
   |      ^^^ actual value is <200>
   |
```

You can then look into the test file at the specified line to figure out what testing condition failed.