# Challenge 3

## Part 1: Make your banking architecture cloud-ready (10 pts)

Deadline: 2024-05-28 12:00 MESZ

The big german bank you are consulting wants to move its new banking systems into the public cloud.
They don't want to maintain a data center on their own anymore after years of contracting IBM to do it for them.

They task you to find a suitable cloud provider (or multiple cloud providers?).

In this iteration of your banking architectzre, try to figure out where to host all of your components.
Come up with an architectural design that answers following questions:

1. Which cloud provider(s) will you choose and why?
2. What product offerings will you choose? Name the products and explain why you think these products fit your requirements most.
    - IaaS: VMs? Storage disks?
    - PaaS: Databases? Serverless products? Managed Kubernetes?
    - For example: _For our Transactions Database we might be using Google Cloud Firestore because it's a managed Document database that can scale easily, is performant and reduces maintenance overhead. But you are paying per transaction, so in high volumes it might not make sense._
3. Do you have any risk of vendor lock-in and if so, how could you reduce that risk?
4. Will the products be performant enough and scale to your use case?
    - For example: Running all infrastructure on a single virtual machine might not result in the performance you need for your banking systems.
5. In what regions will your data be stored?
6. What will all of this cost? (roughly)
    - Consider computing, storage and networking costs. Or costs of your managed services.

To answer questions about scalability, performance and costs, consider the expected load on the systems:

```
2 million customers with roughly 2.5 million accounts, but adjust for customers and accounts that might be coming in the future.
2.4 billion transactions processed so far, with up to 1.4 million new transactions per day.
In total, customers perform on average 20 million requests per day against the systems through their apps (Online Banking & Mobile App).
On month start and end, the bank expects up to 10 times more traffic on its systems than ususal. The systems need to be able to scale for that.
```

This challenge requires some research on your side. Explore the product offerings of different cloud providers
and go with what you think might work best.

Some inspirational input:
- Hyperscalers: AWS, Google Cloud, ~~Azure~~ (don't use Azure, seriously)
- Edge cloud providers: Cloudflare, Fastly
- Managed services providers: Supabase, Fly.io, Netlify
- IaaS / Cloud providers: Hetzner, DigitalOcean

Prepapre a form-free 5 minute presentation of your cloud architecture for our next session and be ready to answer questions about it.

## Part 2: Deploy a Trading Bot to the cloud (10 pts)

Deadline: 2024-06-18

Now that you have a functional Trading API, let's start using it. Your task is to create a trading algorithm and a bot that implements this algorithm.

These are the functional requirements for your trading algoritm:
- Runs at least once a day.
- Utilizes at least one dynamic data point to determine whether to perform a trade.
    - You can pull data from whatever APIs you want.
- The trading algorithms should output an order resource that could be posted to the Trading API or simply output "No trading right now" if it determines to not trade.
    - Since you don't have a live Trading API, the Trading Bot should only output its order as log for now.

The order resource should look like this:

```json
{
  "assetType": "CRYPTO",
  "symbol": "BTC",
  "amount": 1,
  "side": "BUY",
  "orderType": "MARKET"
}
```

where you replace `symbol`, `amount` and `side` with the respective values.

For example, "if the BTC price is below 1 USD then buy 1 BTC" would be a valid trading algorithm, but you can surely do better than that.

Implement this trading algorithm and deploy it as Trading Bot to Google Cloud Functions.

These are the non-functional requirements for the Trading Bot:
- Deployed as serverless Google Cloud Function to your team's Google Cloud project.
- Gets triggered at least once per day through a Google Cloud Scheduler Pub/Sub integration.
- You maintain all your infrastructure resources as code (for example using Terraform).
- The deployment is done automatically through Gitlab CI.
- Your repository's code must not have any secrets (such as API keys or credentials) committed.
  Instead, use the [secrets management of Google Cloud Functions](https://cloud.google.com/functions/docs/configuring/secrets) and Gitlab CI/CD variables.

We are provisioning Google Cloud projects for your team to deploy these resources in.
Write us a PN on Slack to retrieve your credentials and for your Google account to get added to your team's project.

Create a new repository in your team's Gitlab group called `challenge3` where you maintain the code for your function, for your infrastructure and the CI setup.

You can take a look at our [terraform-demo](https://gitlab.com/wwi21amc/terraform-demo) to see how to setup Terraform for Google Cloud and how to deploy a Google Cloud Scheduler.
You'll need to do the rest on your own.

```
Hier einige Lern-Ressourcen, bei denen ihr euch Inspirationen holen könnt.
Vergesst nicht: Uns geht es darum, dass ihr **eine eigene Trading-Strategie entwickelt, die ihr als sinnvoll erachtet**. Es gibt hierfür keine richtige Lösung.

**Trading Bots:**
- [Subreddit /r/algotrading](https://www.reddit.com/r/algotrading/): Viele hilfreiche Posts und Erfahrungsberichte inkl. Anleitungen für Neueinsteiger.
- [Alpha Vantage Academy](https://www.alphavantage.co/academy/): Grundlagen zum Algo-Trading + Tutorials für Trading mit Deep Learning.
- [Building a Trading System in Python | Project Overview auf YouTube](https://www.youtube.com/watch?v=yZ9BBYzoMbs): Anfang einer Tutorial-Reihe für einen Trading-Bot mit guter Einführung in Trading Bots inkl. Anwendungs-Architektur.

**Strategien:**
- [A trader’s guide to quantitative trading von ig.com](https://www.ig.com/en/trading-strategies/a-traders-guide-to-quantitative-trading-200420): Erläuterung von Quantitative Trading und einigen Trading-Strategien.
- [Top 6 Algorithmic Trading Strategies! auf YouTube](https://www.youtube.com/watch?v=5iuF42s6zNo): Simple Erklärung von ein paar Trading-Strategien.
```