#!/bin/bash

# This script creates a new GCP project for a student team for challenge 5
# It enables the necessary Google Cloud APIs
# It also creates a service account named "gitlab-sa" with the roles Cloud Run Admin and Artifact Registry Admin

# Requires you to be authenticated within the Google Cloud CLI

if [[ -z "$1" ]]; then
    echo "Usage: bootstrap-gcp-project.sh <team>, for example: bootstrap-gcp-project.sh wwi21amc-team1"
    exit 1
fi
project=$1

# Create project
gcloud projects create $project

# Link free tier billing account
# We have a billing killer set up for this billing account: https://gitlab.com/andary/dhbw-billing-killer
gcloud beta billing projects link $project --billing-account "0182C6-4BA9AF-29300A"

# Cloud Resource Manager API is required to work with Service Accounts
gcloud services enable cloudresourcemanager.googleapis.com --project $project
gcloud services enable artifactregistry.googleapis.com --project $project
gcloud services enable cloudbuild.googleapis.com --project $project
gcloud services enable run.googleapis.com --project $project
gcloud services enable cloudfunctions.googleapis.com --project $project
gcloud services enable cloudscheduler.googleapis.com --project $project

gcloud iam service-accounts create gitlab-sa --project $project

# Full list of roles is here: https://cloud.google.com/iam/docs/understanding-roles
# These are the roles required to deploy to Cloud Functions and Cloud Run
gcloud projects add-iam-policy-binding $project --member="serviceAccount:gitlab-sa@$project.iam.gserviceaccount.com" --role="roles/iam.serviceAccountUser"
gcloud projects add-iam-policy-binding $project --member="serviceAccount:gitlab-sa@$project.iam.gserviceaccount.com" --role="roles/artifactregistry.admin"
gcloud projects add-iam-policy-binding $project --member="serviceAccount:gitlab-sa@$project.iam.gserviceaccount.com" --role="roles/cloudfunctions.admin"
gcloud projects add-iam-policy-binding $project --member="serviceAccount:gitlab-sa@$project.iam.gserviceaccount.com" --role="roles/run.admin"
gcloud projects add-iam-policy-binding $project --member="serviceAccount:gitlab-sa@$project.iam.gserviceaccount.com" --role="roles/cloudbuild.builds.builder"
gcloud projects add-iam-policy-binding $project --member="serviceAccount:gitlab-sa@$project.iam.gserviceaccount.com" --role="roles/cloudscheduler.admin"
gcloud projects add-iam-policy-binding $project --member="serviceAccount:gitlab-sa@$project.iam.gserviceaccount.com" --role="roles/pubsub.admin"
gcloud projects add-iam-policy-binding $project --member="serviceAccount:gitlab-sa@$project.iam.gserviceaccount.com" --role="roles/storage.admin"

# Use the GCP Dashboard to create keys for the SA